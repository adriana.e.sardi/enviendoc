# Challenge GD4H - EnviEndoc

*For an english translated version of this file, follow this [link](/README.en.md)*

Le <a href="https://gd4h.ecologie.gouv.fr/" target="_blank" rel="noreferrer">Green Data for Health</a> (GD4H) est une offre de service incubée au sein de l’ECOLAB, laboratoire d’innovation pour la transition écologique du Commissariat Général au Développement Durable.

Elle organise un challenge permettant le développement d’outils ancrés dans la communauté de la donnée en santé-environnement afin d’adresser des problématiques partagées.

Liens : 
<a href="https://challenge.gd4h.ecologie.gouv.fr/" target="_blank" rel="noreferrer">Site</a> / 
<a href="https://forum.challenge.gd4h.ecologie.gouv.fr/" target="_blank" rel="noreferrer">Forum</a>
a

## EnviEndoc

Les perturbateurs endocriniens (PE) sont des substances chimiques pouvant avoir un impact négatif sur le système hormonal humain en imitant ou en bloquant l’action des hormones naturelles. Présentes dans de nombreux produits et dans l’environnement, leur impact sanitaire est préoccupant.

Les données sur les PE dans l’environnement sont dispersées et peu visibles. Les bases ne sont pas toutes ouvertes et surtout pas interopérables.

<a href="https://challenge.gd4h.ecologie.gouv.fr/defi/?topic=34" target="_blank" rel="noreferrer">En savoir plus sur le défi</a>

## **Documentation**

>TODO / **Description Solution**

### **Installation**

[Guide d'installation](/INSTALL.md)

### **Utilisation**

>TODO / **documentation d'utilisation de la solution**

### **Contributions**

Si vous souhaitez contribuer à ce projet, merci de suivre les [recommendations](/CONTRIBUTING.md).

### **Licence**

Le code est publié sous licence [MIT](/licence.MIT).

Les données référencés dans ce README et dans le guide d'installation sont publiés sous [Etalab Licence Ouverte 2.0](/licence.etalab-2.0).